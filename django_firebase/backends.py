import json
import requests

from django.contrib.auth.backends import ModelBackend, RemoteUserBackend
from django.core.cache import cache
from django.conf import settings
from django.contrib.auth import get_user_model

import firebase_admin
from firebase_admin import credentials
from firebase_admin.auth import (
    verify_id_token,
    RevokedIdTokenError,
    InvalidIdTokenError,
)

UserModel = get_user_model()

if not(len(firebase_admin._apps)):
    cred = credentials.Certificate(settings.FIREBASE_CERT_KEY)
    default_app = firebase_admin.initialize_app(cred)


def sign_in_with_email_and_password(api_key, email, password):
    """
    Firebaseで認証を行う(SDKの signInWithEmailAndPassword と同値)
    :param api_key:
    :param email:
    :param password:
    :return: Firebaseの、idTokenなどを含んだ、認証情報
    """
    # https://firebase.google.com/docs/reference/rest/auth/#section-sign-in-email-password
    uri = f"https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key={api_key}"
    headers = {"Content-type": "application/json"}
    data = json.dumps({"email": email, "password": password, "returnSecureToken": True})

    result = requests.post(url=uri,
                           headers=headers,
                           data=data)
    return result.json()


class FirebaseAuthenticationEmailBackend(ModelBackend):
    create_unknown_user = True

    def authenticate(self, request, username=None, password=None, **kwargs):
        if username is None:
            username = kwargs.get(UserModel.USERNAME_FIELD)
        if username is None or password is None:
            return

        try:
            id_token = sign_in_with_email_and_password(settings.FIREBASE_API_KEY, username, password)['idToken']
            uid = verify_id_token(id_token)['uid']
        except RevokedIdTokenError:
            return
        except InvalidIdTokenError:
            return
        except KeyError:
            return

        if self.create_unknown_user:
            user, created = UserModel._default_manager.get_or_create(**{
                UserModel.USERNAME_FIELD: uid
            })
            if created:
                user = self.configure_user(request, user)
        else:
            try:
                user = UserModel._default_manager.get_by_natural_key(uid)
            except UserModel.DoesNotExist:
                pass
        return user if self.user_can_authenticate(user) else None

    def configure_user(self, request, user):
        return user


class FirebaseAuthenticationTokenBackend(RemoteUserBackend):
    check_revoked_expired = 3600
    cache_prefix = 'firebase'


    def authenticate(self, request, remote_user):
        if not remote_user:
            return
        user = None

        try:
            username = self.clean_username(remote_user)
        except RevokedIdTokenError:
            return
        except InvalidIdTokenError:
            return

        if self.create_unknown_user:
            user, created = UserModel._default_manager.get_or_create(**{
                UserModel.USERNAME_FIELD: username
            })
            if created:
                user = self.configure_user(request, user)
        else:
            try:
                user = UserModel._default_manager.get_by_natural_key(username)
            except UserModel.DoesNotExist:
                pass
        return user if self.user_can_authenticate(user) else None


    def clean_username(self, username):
        id_token = username.lstrip('Bearer').strip()

        uid = verify_id_token(id_token)['uid']

        cache_key = f'{self.cache_prefix}:{uid}'
        check = not cache.get(cache_key)
        verify_id_token(id_token, check_revoked=check)
        if check:
            cache.set(cache_key, 'registered', self.check_revoked_expired)

        return uid


class AllowAllUsersFirebaseAuthenticationTokenBackend(FirebaseAuthenticationTokenBackend):
    def user_can_authenticate(self, user):
        return True
