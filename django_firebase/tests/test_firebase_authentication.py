from datetime import datetime

from django.conf import settings
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.test import TestCase, modify_settings, override_settings
from django.utils import timezone

from django_firebase.middleware import FirebaseAuthenticationTokenMiddleware
from django_firebase.backends import (
    sign_in_with_email_and_password,
    FirebaseAuthenticationTokenBackend,
)

'''
Create Firebase Auth User and write credencials info to settings.py.

```
FIREBASE_API_KEY = 'firebase-sdk-api-key'
FIREBASE_USER1_UID = 'user1-uid'
FIREBASE_USER1_EMAIL = 'user1-email@exsample.com'
FIREBASE_USER1_PASSWORD = 'user1-password'
FIREBASE_USER2_UID = 'user2-uid'
FIREBASE_USER2_EMAIL = 'user2-email@exsample.com'
FIREBASE_USER2_PASSWORD = 'user2-password'
```
'''


@override_settings(ROOT_URLCONF='django_firebase.tests.urls')
class FirebaseAuthenticationTest(TestCase):

    middleware = 'django_firebase.middleware.FirebaseAuthenticationTokenMiddleware'
    backend = 'django_firebase.backends.FirebaseAuthenticationTokenBackend'
    header = 'HTTP_AUTHORIZATION'
    email_header = 'REMOTE_EMAIL'

    # Usernames to be passed in REMOTE_USER for the test_known_user test case.
    id_token1 = 'Bearer ' + sign_in_with_email_and_password(
        settings.FIREBASE_API_KEY,
        settings.FIREBASE_USER1_EMAIL,
        settings.FIREBASE_USER1_PASSWORD)['idToken']
    id_token2 = 'Bearer ' + sign_in_with_email_and_password(
        settings.FIREBASE_API_KEY,
        settings.FIREBASE_USER2_EMAIL,
        settings.FIREBASE_USER2_PASSWORD)['idToken']

    def setUp(self):
        self.patched_settings = modify_settings(
            AUTHENTICATION_BACKENDS={'append': self.backend},
            MIDDLEWARE={'append': self.middleware},
        )
        self.patched_settings.enable()

    def tearDown(self):
        self.patched_settings.disable()

    def test_no_remote_user(self):
        """
        Tests requests where no remote user is specified and insures that no
        users get created.
        """
        num_users = User.objects.count()

        response = self.client.get('/remote_user/')
        self.assertTrue(response.context['user'].is_anonymous)
        self.assertEqual(User.objects.count(), num_users)

        response = self.client.get('/remote_user/', **{self.header: None})
        self.assertTrue(response.context['user'].is_anonymous)
        self.assertEqual(User.objects.count(), num_users)

        response = self.client.get('/remote_user/', **{self.header: ''})
        self.assertTrue(response.context['user'].is_anonymous)
        self.assertEqual(User.objects.count(), num_users)

    def test_unknown_user(self):
        """
        Tests the case where the username passed in the header does not exist
        as a User.
        """
        num_users = User.objects.count()
        response = self.client.get('/remote_user/', **{self.header: self.id_token1})
        self.assertEqual(response.context['user'].username, settings.FIREBASE_USER1_UID)
        self.assertEqual(User.objects.count(), num_users + 1)
        User.objects.get(username=settings.FIREBASE_USER1_UID)

        # Another request with same user should not create any new users.
        response = self.client.get('/remote_user/', **{self.header: self.id_token1})
        self.assertEqual(User.objects.count(), num_users + 1)

    def test_known_user(self):
        """
        Tests the case where the username passed in the header is a valid User.
        """
        User.objects.create(username=settings.FIREBASE_USER1_UID)
        User.objects.create(username=settings.FIREBASE_USER2_UID)
        num_users = User.objects.count()
        response = self.client.get('/remote_user/',
                                   **{self.header: self.id_token1})
        self.assertEqual(response.context['user'].username, settings.FIREBASE_USER1_UID)
        self.assertEqual(User.objects.count(), num_users)
        # A different user passed in the headers causes the new user
        # to be logged in.
        response = self.client.get('/remote_user/',
                                   **{self.header: self.id_token2})
        self.assertEqual(response.context['user'].username, settings.FIREBASE_USER2_UID)
        self.assertEqual(User.objects.count(), num_users)

    def test_last_login(self):
        """
        A user's last_login is set the first time they make a
        request but not updated in subsequent requests with the same session.
        """
        user = User.objects.create(username=settings.FIREBASE_USER1_UID)
        # Set last_login to something so we can determine if it changes.
        default_login = datetime(2000, 1, 1)
        if settings.USE_TZ:
            default_login = default_login.replace(tzinfo=timezone.utc)
        user.last_login = default_login
        user.save()

        response = self.client.get('/remote_user/',
                                   **{self.header: self.id_token1})
        self.assertNotEqual(default_login, response.context['user'].last_login)

        user = User.objects.get(username=settings.FIREBASE_USER1_UID)
        user.last_login = default_login
        user.save()
        response = self.client.get('/remote_user/',
                                   **{self.header: self.id_token1})
        self.assertEqual(default_login, response.context['user'].last_login)

    def test_header_disappears(self):
        """
        A logged in user is logged out automatically when
        the REMOTE_USER header disappears during the same browser session.
        """
        User.objects.create(username=settings.FIREBASE_USER1_UID)
        # Known user authenticates
        response = self.client.get('/remote_user/',
                                   **{self.header: self.id_token1})
        self.assertEqual(response.context['user'].username, settings.FIREBASE_USER1_UID)
        # During the session, the REMOTE_USER header disappears. Should trigger logout.
        response = self.client.get('/remote_user/')
        self.assertTrue(response.context['user'].is_anonymous)
        # verify the FirebaseAuthentication middleware will not remove a user
        # authenticated via another backend
        User.objects.create_user(username='modeluser', password='foo')
        self.client.login(username='modeluser', password='foo')
        authenticate(username='modeluser', password='foo')
        response = self.client.get('/remote_user/')
        self.assertEqual(response.context['user'].username, 'modeluser')

    def test_user_switch_forces_new_login(self):
        """
        If the username in the header changes between requests
        that the original user is logged out
        """
        User.objects.create(username=settings.FIREBASE_USER1_UID)
        # Known user authenticates
        response = self.client.get('/remote_user/',
                                   **{self.header: self.id_token1})
        self.assertEqual(response.context['user'].username, settings.FIREBASE_USER1_UID)
        # During the session, the REMOTE_USER changes to a different user.
        response = self.client.get('/remote_user/',
                                   **{self.header: self.id_token2})
        # The current user is not the prior remote_user.
        # In backends that create a new user, username is "newnewuser"
        # In backends that do not create new users, it is '' (anonymous user)
        self.assertNotEqual(response.context['user'].username, settings.FIREBASE_USER1_UID)

    def test_inactive_user(self):
        User.objects.create(username=settings.FIREBASE_USER1_UID, is_active=False)
        response = self.client.get('/remote_user/', **{self.header: self.id_token1})
        self.assertTrue(response.context['user'].is_anonymous)


class FirebaseAuthenticationNoCreateBackend(FirebaseAuthenticationTokenBackend):
    """Backend that doesn't create unknown users."""
    create_unknown_user = False


class FirebaseAuthenticationNoCreateTest(FirebaseAuthenticationTest):
    """
    Contains the same tests as FirebaseAuthenticationTest, but using a custom auth backend
    class that doesn't create unknown users.
    """

    backend = 'django_firebase.tests.test_firebase_authentication.FirebaseAuthenticationNoCreateBackend'

    def test_unknown_user(self):
        num_users = User.objects.count()
        response = self.client.get('/remote_user/', **{self.header: 'unknownuser'})
        self.assertTrue(response.context['user'].is_anonymous)
        self.assertEqual(User.objects.count(), num_users)


class AllowAllUsersFirebaseAuthenticationTokenBackendTest(FirebaseAuthenticationTest):
    """Backend that allows inactive users."""
    backend = 'django_firebase.backends.AllowAllUsersFirebaseAuthenticationTokenBackend'

    def test_inactive_user(self):
        user = User.objects.create(username=settings.FIREBASE_USER1_UID, is_active=False)
        response = self.client.get('/remote_user/', **{self.header: self.id_token1})
        self.assertEqual(response.context['user'].username, user.username)
