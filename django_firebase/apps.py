from django.apps import AppConfig


class DjangoFirebaseConfig(AppConfig):
    name = 'django_firebase'
