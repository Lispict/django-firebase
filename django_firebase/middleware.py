from django.contrib.auth.middleware import RemoteUserMiddleware

class FirebaseAuthenticationTokenMiddleware(RemoteUserMiddleware):
    header = 'HTTP_AUTHORIZATION'
