# Configration

RemoteUserBackend 準拠

settings.py
```
MIDDLEWARE = [
    '...',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django_firebase.middleware.FirebaseAuthenticationTokenMiddleware',
    '...',
]

AUTHENTICATION_BACKENDS = [
    'django_firebase.backends.FirebaseAuthenticationTokenBackend',
    'django_firebase.backends.FirebaseAuthenticationEmailBackend',
    'django.contrib.auth.backends.ModelBackend',
]

FIREBASE_API_KEY = 'firebase-sdk-api-key'
FIREBASE_CERT_KEY = '/path/to/firebase-app-adminsdk-cert.json'
```

# Use Cache

キャッシュ利用することで、id_token の check_revoked にインターバルを設ける
